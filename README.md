# goodreads-books

In this project, I concern about EDA or **Exploratory Data Analysis**. 
I have some question based on the Data that I got in this [**link**](https://www.kaggle.com/jealousleopard/goodreadsbooks). The question are :
1. How many books published over the year?
2. In what language books much published?
3. Who is the most productive authors?
4. The highest ratings books going to?
5. The highest review books going to?
6. Usually how many pages that book have with good ratings?
7. How many books with top ratings published over the year?
8. Who is the best authors based on the ratings?
9. What is the best publisher based on the ratings?

Before answer the question I want to see the general distribution of dataset in Goodread Book.
![Screenshot](Data Distribution of Goodread Book.png)

and the answer of the question are:
1. ![Screenshot](Sum of Book Published Over the Year.png)
2. ![Screenshot](Top Ten Widely Language Used in Books.png)
3. ![Screenshot](Most Productive Authors.png)
4. ![Screenshot](Top Ten Highest Rating of Books.png)
5. ![Screenshot](Most Reviews Books.png)
6. ![Screenshot](Pages Book with Good Ratings.png)
7. ![Screenshot](Sum of Book with top Rated Over The Year.png)
8. ![Screenshot](Author with Good Ratings.png)
9. ![Screenshot](Publisher with Good Ratings.png)
 

